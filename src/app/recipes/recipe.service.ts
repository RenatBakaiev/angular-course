import {Recipe} from "./recipe.model";
import {Injectable} from "@angular/core";
import {Ingredient} from '../shared/ingredient.model'
import {ShoppingListService} from "../shopping-list/shopping-list.service";
import {Subject} from "rxjs";

@Injectable()
export class RecipeService {
  recipeChanged = new Subject<Recipe[]>();

  // private recipes: Recipe[] = [
  //   new Recipe(
  //     'Tasty Pizza',
  //     'A super-tasty Pizza - just Awesome',
  //     'https://i.obozrevatel.com/food/recipemain/2019/2/17/ap.webp?size=600x400',
  //     [
  //       new Ingredient('Meat', 3),
  //       new Ingredient('Cheese', 5),
  //     ]),
  //   new Recipe(
  //     'Super Bread',
  //     'This thing is super',
  //     'https://www.melskitchencafe.com/wp-content/uploads/french-bread2.jpg',
  //     [
  //       new Ingredient('Eggs', 6),
  //       new Ingredient('Flour', 1),
  //     ])
  // ];

  private recipes: Recipe[] = [];

  constructor(private slService: ShoppingListService) {
  }

  setRecipes(recipes: Recipe[]) {
    this.recipes = recipes;
    this.recipeChanged.next(this.recipes.slice())
  }

  getRecipes() {
    return this.recipes.slice();
  }

  getRecipe(index: number) {
    return this.recipes[index];
  }

  addIngredientsToShoppingList(ingredients: Ingredient[]) {
    this.slService.addIngredients(ingredients);
  }

  addRecipe(recipe: Recipe) {
    this.recipes.push(recipe);
    this.recipeChanged.next(this.recipes.slice());
  }

  updateRecipe(index: number, newRecipe: Recipe) {
    this.recipes[index] = newRecipe;
    this.recipeChanged.next(this.recipes.slice());
  }

  deleteRecipe(index: number) {
    this.recipes.splice(index, 1);
    this.recipeChanged.next(this.recipes.slice());
  }
}
